package com.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileReader {

    public String read(String filename) {
        try {
            return new String(Files.readAllBytes(Paths.get(filename)));
        } catch (IOException e) {
            throw new RuntimeException("File read error, name [" + filename + "]");
        }
    }
    public String write(String filename){
        try{
            return new String (Files.write(Paths.get("./output.txt"), "Information string herer".getBytes()));
        }catch(IOException e){
            throw new RuntimeException("File read error, name [" + filename + "]");
        }
    }
}
