package labs.concurency.sum;

import java.util.concurrent.atomic.AtomicInteger;

public class Sum {

    private AtomicInteger sum = new AtomicInteger(0);

    public void calculate() {
        sum.addAndGet(1);
    }

    public void substract() {
        sum.addAndGet(-1);
    }

    public int getSum() {
        return sum.get();
    }

    public void setSum(int sum) {
        this.sum.set(sum);
    }
}
