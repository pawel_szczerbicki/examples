package labs.concurency.sda;

public class Printer implements Runnable {

   private int a;

    public Printer(int a) {
        this.a = a;
    }

    @Override
    public void run() {
         a++;
        System.out.println(a);
    }
}
