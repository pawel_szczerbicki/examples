package pwr.databases.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by pawel on 07.12.15.
 */
@Service
public class UserService {

    private String asyncMsg = "in progress";

    @Autowired
    private UserRepository userRepository;

    public List<User> getAll() {
        return userRepository.getAll();
    }

    public void save(User u) {
        userRepository.add(u);
    }

    @Async
    public void doAsync() {
        System.out.println("start async");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        asyncMsg = "done";
        System.out.println("finish async");
    }

    public String getAsyncMsg() {
        return asyncMsg;
    }
}
