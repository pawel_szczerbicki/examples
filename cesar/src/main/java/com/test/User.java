package com.test;

public class User {

    private String name;

    private String surname;

    private String login;

    private String password;

    User(String name, String surname, String login) {
        this.name = name;
        this.surname = surname;
        this.login = login;
    }

    public String getFullName() {
        return "My name is " + name + " " + surname;
    }

    public String getLogin() {
        return login;
    }
}
