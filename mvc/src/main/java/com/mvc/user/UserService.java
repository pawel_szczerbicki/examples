package com.mvc.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public void save(User u) {
        userDao.add(u);
    }

    public List<User> getAll() {
        return userDao.getAll();
    }
}
