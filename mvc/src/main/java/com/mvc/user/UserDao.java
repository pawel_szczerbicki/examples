package com.mvc.user;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDao {

    private List<User> users = new ArrayList<>();

    public void add(User u) {
        users.add(u);
    }

    public List<User> getAll() {
        return users;
    }
}
