package sda.example.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class UserService {

    @Autowired
    private UserDao userDao;

    public void save (User u ){
        u.setName(u.getName() + "test");
        userDao.save(u);
    }

    public List<User> getAll(){
       return userDao.getAll();
    }


}
