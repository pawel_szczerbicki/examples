package labs.concurency;

public class ThreadExample {


    public static void main(String[] args) throws InterruptedException {
        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                System.out.println("runnable 1");

            }
        };
        Runnable r2 = () -> System.out.println("runnable 2");


        Thread t1 = new Thread(r1);
        Thread t2 = new Thread(r2);

        t1.start();
        t2.start();

        t1.join();

        t2.join();

        System.out.println("done");

    }
}
