package com.sda.dropbox.listener;

import com.sda.dropbox.config.ConfigService;
import com.sda.dropbox.stats.StatsHolder;
import com.sda.dropbox.upload.Uploader;

import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.sda.dropbox.config.Keys.DIRECTORY;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;

public class DirectoryListener {

    private final Uploader uploader;
    private final String dir;
    private final ExecutorService executor = Executors.newCachedThreadPool();
    private StatsHolder holder; // TODO inject stats holder

    public DirectoryListener(Uploader uploader, ConfigService cfg) {
        this.uploader = uploader;
        this.dir = cfg.get(DIRECTORY);
    }

    public void listen() {
        try {
            WatchService watchService = FileSystems.getDefault().newWatchService();
            Path path = Paths.get(dir);
            path.register(watchService, ENTRY_CREATE);
            WatchKey key;
            while ((key = watchService.take()) != null) {
                String name = key.pollEvents().get(0).context().toString();
                System.out.println(name);
                executor.submit(() -> {
                    //TODO move lambda to separate class
                    uploader.upload(dir + name, name);
                   holder.add();
                });
                key.reset();
            }
        } catch (IOException | InterruptedException e) {
            throw new ListenerException("Can not listen to directory " + dir, e);
        }
    }
}
