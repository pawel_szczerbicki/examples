package com.sda.composition;

public class Date {
    private int month;
    private int year;
    private int day;

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public int getDay() {
        return day;
    }
}
