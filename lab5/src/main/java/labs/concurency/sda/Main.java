package labs.concurency.sda;

import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    AtomicInteger val = new AtomicInteger(0);

    public static void main(String[] args) throws InterruptedException {
        new Main().runMain();
    }

    public void runMain() throws InterruptedException {
       while(true){
        Runnable runnable = () -> {
            val.addAndGet(1) ;
        };


        Thread thread = new Thread(runnable);
        Thread thread1 = new Thread(runnable);

        thread.start();
        thread1.start();

        thread.join();
        thread1.join();

        if (val.get() >1)
            System.out.println(val);}
    }
}
