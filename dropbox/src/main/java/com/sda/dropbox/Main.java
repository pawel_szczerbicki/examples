package com.sda.dropbox;

import com.sda.dropbox.config.ConfigService;
import com.sda.dropbox.email.ClientFactory;
import com.sda.dropbox.email.client.EmailClient;
import com.sda.dropbox.listener.DirectoryListener;
import com.sda.dropbox.stats.StatsService;
import com.sda.dropbox.upload.DropBoxUploader;
import com.sda.dropbox.upload.Uploader;

import java.io.IOException;

import static java.util.concurrent.Executors.newSingleThreadExecutor;

public class Main {
    public static final int MAX_PILE_SIUZE = 15;
    private static final int PROPS_INDEX = 0;

    public static void main(String[] args) throws IOException {
        String propsPath = args[PROPS_INDEX];
        ConfigService cfg = new ConfigService(propsPath).load();
        EmailClient emailClient = ClientFactory.create(cfg);
        Uploader u = new DropBoxUploader(cfg, emailClient);
        DirectoryListener listener = new DirectoryListener(u, cfg);
        newSingleThreadExecutor().submit(new StatsService());
        newSingleThreadExecutor().submit(listener::listen);
    }

    public void robot() {
        String commands = "LLLLPPPLLMMPMPLPLPLPLPLDSFVDSFDS";
        int pos = 0;
        boolean holdsBlock = false;
        int piles[] = new int[10];

        for (char c : commands.toCharArray()) {
            if (c == 'l' && piles[pos] < MAX_PILE_SIUZE && holdsBlock) {
                piles[pos] = +1;
                holdsBlock = false;
            } else if (c == 'm' && pos < 10) {
                pos++;
            } else if (c == 'p') {
                holdsBlock = true;
                pos = 0;
            }
        }
    }
}
