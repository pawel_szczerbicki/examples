package labs.concurency;

/**
 * Created by pawel on 26.11.15.
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        while (true) {
            Account account1 = new Account("Jonh", 1);
            Account account2 = new Account("Michael", 1);

            Thread t = new Thread(getRunnable(account1, account2));
            Thread t2 = new Thread(getRunnable(account1, account2));

            t2.start();
            t.start();
            t.join();
            t2.join();
        }
    }
    private static Runnable getRunnable(Account account1, Account account2) {
        return () -> {
            synchronized (account1) {
                synchronized (account2) {
                    if (account1.getAmount() > 0) {
                        account2.add(1);
                        account1.withdraw(1);
                    }
                }
            }
        };
    }
}
