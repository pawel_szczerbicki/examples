package pwr.databases.user;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by pawel on 07.12.15.
 */

@Repository
public class UserRepository {

    private MongoTemplate template;

    public List<User> getAll() {
        return template.findAll(User.class);
    }

    public void add(User u) {
      template.save(u);
    }
}
