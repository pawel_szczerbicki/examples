package com.mvc;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController2 {

    @GetMapping("/")
    public String index(){
        return "index";
    }
}
