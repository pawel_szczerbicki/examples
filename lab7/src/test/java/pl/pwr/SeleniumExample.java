package pl.pwr;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import pwr.common.CommonPeselValidator;

import static org.junit.Assert.assertNotNull;
import static org.openqa.selenium.By.id;

public class SeleniumExample {

    private WebDriver driver;

    private Wait<WebDriver> wait;

    @Before
    public void init() {
        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver, 30);
        driver.get("http://google.com");
    }

    @After
    public void tearDown() {
        driver.close();
    }

    @Test
    public void shouldSearchWithGoogle() {
        CommonPeselValidator v = new CommonPeselValidator();
        WebElement element = driver.findElement(id("lst-ib"));
        element.sendKeys("selenium");
        element.submit();
        wait.until(driver->driver.findElement(id("extabar"))!=null);
        assertNotNull(driver.findElement(By.partialLinkText("Selenium wedefdds")));
    }


}
