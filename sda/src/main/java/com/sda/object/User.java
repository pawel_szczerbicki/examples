package com.sda.object;

public class User {

    public String name;

    public String surname;

    public static void main(String[] args) {
        new User().some();
    }

    int some() {
        try {
            return 1;
        } catch (Exception e) {
            return 2;
        } finally {
            return 3;
        }

    }

    class Car {
    }
}
