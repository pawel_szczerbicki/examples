package pl.edu.pwr;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Comparator;
import java.util.Properties;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.Comparator.reverseOrder;
import static pl.edu.pwr.config.Keys.SITE_URL;

public class Main {
    public static final int PROPERTIES_FILE = 0;

    Properties testProperties = new Properties();

    public static void main(String[] args) {
        findHighest();
    }

    private static void findHighest() {
        try {
            MessageType.valueOf("some");

        } catch (IllegalArgumentException e) {

        }

        String digits = "11,22,123";


        stream(digits.split(",")).map(s -> asList(s.split(""))).peek(e -> e.sort(reverseOrder()))
                .flatMap(s->s.stream().map(p->p + ",")).collect(Collectors.joining());

        asList(digits.split(",")).forEach(d -> System.out.println(asList(d.split(""))
                .stream().sorted(Comparator.<String>reverseOrder())
                .collect(Collectors.joining())));
    }

    public void run(String[] args) {
        try {
            testProperties.load(new FileInputStream(args[PROPERTIES_FILE]));
            System.out.println(testProperties.getProperty(SITE_URL));
        } catch (IOException e) {
            System.out.println("no file");
        }
    }


}
