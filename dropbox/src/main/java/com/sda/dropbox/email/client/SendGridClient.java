package com.sda.dropbox.email.client;

import com.sda.dropbox.config.ConfigService;
import com.sda.dropbox.email.SendingEmailException;
import com.sendgrid.*;

import java.io.IOException;

import static com.sda.dropbox.config.Keys.SENDGRID_KEY;

public class SendGridClient implements EmailClient {

    private final ConfigService cfg;

    public SendGridClient(ConfigService cfg) {
        this.cfg = cfg;
    }

    @Override
    public void send(com.sda.dropbox.email.Email e) {

        //TODO move to properties
        Email from = new Email("mafo@alexbox.online");
        Email to = new Email(e.getTo());
        Content content = new Content("text/plain", e.getContent());
        Mail mail = new Mail(from, e.getSubject(), to, content);

        SendGrid sg = new SendGrid(cfg.get(SENDGRID_KEY));
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            sg.api(request);
        } catch (IOException ex) {
            throw new SendingEmailException("Could not send email to: " + e.getTo(), ex);
        }

    }
}
