package pwr.databases;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pwr.databases.user.UserService;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
public class IndexController {

    @Autowired
    private UserService service;

    @RequestMapping(value = "/", method = GET)
    @ResponseBody
    public String index() {
        return "<b>Hello fdfdsfds</b>";
    }

    @RequestMapping("/index")
    public String some(Model model){
        service.doAsync();
        model.addAttribute("msg", service.getAsyncMsg());
        return "index";
    }

    @RequestMapping("/async-status")
    public String asyncStatus(Model model){
        model.addAttribute("msg", service.getAsyncMsg());
        return "index";
    }
}
