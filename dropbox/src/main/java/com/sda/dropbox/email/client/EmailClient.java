package com.sda.dropbox.email.client;

import com.sda.dropbox.email.Email;

public interface EmailClient {

    void send(Email e);
}
