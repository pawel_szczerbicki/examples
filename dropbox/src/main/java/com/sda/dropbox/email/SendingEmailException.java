package com.sda.dropbox.email;

public class SendingEmailException extends RuntimeException {

    public SendingEmailException(String message, Throwable cause) {
        super(message, cause);
    }
}
