package com.mvc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

@Configuration
@ComponentScan(basePackages = "com.mvc")
@EnableWebMvc
//@EnableAsync
public class AppConfig extends WebMvcConfigurerAdapter {
    public AppConfig() {
    }

    //
//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**").allowedMethods("*").allowedOrigins("*");
//    }
//
    @Bean
    public UrlBasedViewResolver urlBasedViewResolver(){
        UrlBasedViewResolver r = new UrlBasedViewResolver();
        r.setViewClass(JstlView.class);
        r.setPrefix("/jsp/");
        r.setSuffix(".jsp");
        return r;
    }

//    @Bean
//    public MongoTemplate mongoTemplate() throws UnknownHostException {
//        MongoCredential credential = createCredential("admin", "dev", "admin".toCharArray());
//        MongoClient c = new MongoClient(new ServerAddress("mongolab.com", 27017), singletonList(credential));
//        return new MongoTemplate(new SimpleMongoDbFactory(c,"dev"));
//    }

}
