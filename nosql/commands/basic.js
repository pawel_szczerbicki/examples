//Insert vs update
db.getCollection('some').insert({
    "_id": ObjectId("5cafa04bdc4ebfed86b78d58"),
    "a": "dsasd"
});

//Criteria
db.drivers.find({"points": 5});
db.drivers.find({"points": {$gt: 20}});
db.drivers.count({"last_name": "Kowalski"});
db.drivers.count({"age": 18, "points": {$gt: 0}});
db.drivers.remove({"_id": ObjectId("5b5e2f89529592926f70f022")});
db.drivers.remove({"points": {$gt: 20}})
db.drivers.update({"_id": ObjectId("5b5e2f89529592926f70f022")}, {$set: {"points": 17}});


//SORT skip limit
db.drivers.find({"points": {$gt: 20}}) .sort({"points": -1}).skip(2) .limit(10)

//in nin
db.drivers.find({"first_name": { $in: ["Emi", "Adam", "Ania"] }})
db.drivers.remove({"age": {$lt : 20.0}})



//UPDATE
db.drivers.update(
  {"_id" : ObjectId("5b5e2f89529592926f70f022")}, {$set: {"last_name": "Kowalska"}}
)
db.drivers.updateMany( {"points": 8}, {$set: {"points": 4}} )

db.drivers.updateMany({"first_name" : {$in: ["Paweł", "Anna", "Jan"]}}, {$inc: {"age": -1}})
db.drivers.remove({"points" : {$gte : 21.0}})


//ADDITIONAL
db.drivers.find({"supervision": { $exists: true }})
db.drivers.find({"vehicles": { $size: 2 }})
db.drivers.find({"vehicles": { $size: 4 }}).sort({"points": -1})

db.drivers.find({ "first_name": "Grzegorz", $or : [
        {"age": {$gte: 18.0, $lte: 20.0}},
        {"age": {$gte: 80.0, $lte: 100.0}} ]
})

db.drivers.updateMany(
  {"first_name" : "Paweł", "points" : {lt: 15}}, {$set: {"points": 15}}
)
