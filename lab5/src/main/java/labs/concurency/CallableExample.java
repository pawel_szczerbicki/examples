package labs.concurency;

import java.util.concurrent.*;

/**
 * Created by pawel on 26.11.15.
 */
public class CallableExample {

    private static ExecutorService executor = Executors.newFixedThreadPool(3);

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        Callable<Integer>  c = () -> 1+1;
        Future<Integer> submit = executor.submit(c);

        //JAKIS KOD

        int sum = submit.get();
    }
}
