package sda.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import sda.example.user.UserDao;
import sda.example.user.UserService;

@Component
public class Bootstrap {

    @Autowired
    private UserService userService;

    @Autowired
    private UserDao userDao;


    @Autowired
    public Bootstrap(UserDao u) {
        userDao.getAll();
    }

    public static void main(String[] args) {
        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(SdaContex.class);


    }
}
