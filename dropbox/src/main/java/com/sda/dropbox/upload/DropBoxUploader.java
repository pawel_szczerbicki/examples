package com.sda.dropbox.upload;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.sda.dropbox.config.ConfigService;
import com.sda.dropbox.email.Email;
import com.sda.dropbox.email.client.EmailClient;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.sda.dropbox.config.Keys.*;
import static java.lang.String.format;

public class DropBoxUploader implements Uploader {

    private final ConfigService cfg;
    private final EmailClient client;

    public DropBoxUploader(ConfigService cfg, EmailClient client) {
        this.cfg = cfg;
        this.client = client;
    }

    @Override
    public void upload(String path, String name) {
        String token = cfg.get(DROPBOX_KEY);
        DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/java-tutorial").build();
        DbxClientV2 dbClient = new DbxClientV2(config, token);

        try (InputStream in = new FileInputStream(path)) {
            dbClient.files().uploadBuilder("/" + name)
                    .uploadAndFinish(in);

            //TODO move to external class. ex emailService.onNewFileUploaded(filename)
            String content = format(cfg.get(EMAIL_CONTENT), name);
            client.send(new Email(cfg.get(EMAIL_SUBJECT), content, cfg.get(EMAIL_TO)));
        } catch (IOException | DbxException e) {
            throw new UploadException("Can not upload file " + path, e);
        }
    }
}
