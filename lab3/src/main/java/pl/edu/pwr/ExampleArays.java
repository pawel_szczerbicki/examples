package pl.edu.pwr;

public class ExampleArays {

    public static void main(String[] args) {
        isEven(20);
        int[] digits = {1, 2, 3, 4, 5, 6, 7};
        String[] digitsStr = {"1", "2", "2222"};
        for (int digit : digits)
            if (isEven(digit)) System.out.println(digit);

        for (String s : digitsStr) {
            if (isEven(s)) System.out.printf(s);
        }
    }

    static boolean isEven(int digit) {
        return digit % 2 == 0;
    }

    static boolean isEven(String digit) {
        int parsedDigit = Integer.parseInt(digit);
        return parsedDigit % 2 == 0;
    }
}
