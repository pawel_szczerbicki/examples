package com.sda.dropbox.listener;

public class ListenerException extends RuntimeException {

    public ListenerException(String message, Throwable cause) {
        super(message, cause);
    }
}
