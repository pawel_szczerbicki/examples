package com.sda.reader;

import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;

public class User {
    private static final int DAYS_TO_EXPIRE_PASSWORD = 30;
    private String name;
    private String surname;
    private String pesel;
    private String login;
    private String password;
    private boolean active;
    private LocalDateTime lastPasswordChange;

    public User(String pesel, String login, String password) {
        this.pesel = pesel;
        this.login = login;
        setPassword(password);
    }

    //wlasna linkedlisrt

    public User(String pesel, String login, String password, String name) {
        this(pesel, login, password);
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPassword(String password) {
        this.password = password;
        lastPasswordChange = now();
    }

    public boolean isActive() {
        return active;
    }

    public boolean isPasswordExpired() {
        return lastPasswordChange.plusDays(DAYS_TO_EXPIRE_PASSWORD)
                .isBefore(now());
    }

    @Override
    public boolean equals(Object o) {
        User u = (User) o;
        return this.surname.equals(u.surname);
    }

    @Override
    public int hashCode() {
        return surname.hashCode();
    }

    public static void main(String[] args) {
        User u1 = new User("1", "1","1");
        User u2 = new User("2", "2","2");

        System.out.println(u1.equals(u2));
    }
}
