package com.sda.dropbox.email;

import com.sda.dropbox.config.ConfigService;
import com.sda.dropbox.email.client.EmailClient;
import com.sda.dropbox.email.client.MailJetClient;
import com.sda.dropbox.email.client.SendGridClient;

import static com.sda.dropbox.config.Keys.EMAIL_CLIENT;

public class ClientFactory {

    //TODO move to enum
    private static final String MAILJET = "mailjet";
    private static final String SENDGRID = "sendgrid";

    public static EmailClient create(ConfigService cfg) {
        String client = cfg.get(EMAIL_CLIENT);
        if (client.equals(MAILJET))
            return new MailJetClient();
        else if (client.equals(SENDGRID))
            return new SendGridClient(cfg);
        else throw new RuntimeException("No such client available");
    }
}
