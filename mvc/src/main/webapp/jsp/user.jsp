<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>

My ip is: ${ip}
<h1>Users</h1>
<c:forEach items="${users}" var="user">
   <h4>${user.email}, ${user.name}</h4>
</c:forEach>


<h4>Dodaj usera</h4>
<form action="/users" method="post">
     <input placeholder="Username" name="username">
     <input placeholder="Name" name="name">
     <input placeholder="Email" name="email">
    <button type="submit">Save</button>
</form>
</body>
</html>