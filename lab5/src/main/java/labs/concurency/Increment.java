package labs.concurency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Increment {
    int count = 0;
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(2);

//        IntStream.range(0, 10000)
//                .forEach(i -> executor.submit(this::increment));

        executor.isShutdown();
        executor.awaitTermination(100, TimeUnit.SECONDS);

//        System.out.println(count);
    }

//    public void increment(){
//        void increment() {
//            count = count + 1;
//    }
}
