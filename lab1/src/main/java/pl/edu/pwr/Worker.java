package pl.edu.pwr;

import pl.edu.pwr.reader.AbstractReader;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Created by Pawel on 2014-10-24.
 */
public class Worker {
    private AbstractReader reader;

    private static List<String> links;

    static {
        links = new ArrayList<>();
        links.add("www.google.com");
        links.add("/articles");
        links.add("http://some.com");
        links.add("bad url");
    }

    public Worker(AbstractReader reader) {
        this.reader = reader;
    }

    public void start() {


    }

    private List<String> findExternalLinksWithStandardLoop() {
        List<String> external = new ArrayList<>();
        for (int i = 0; i < links.size(); i++)
            if (links.get(i).startsWith("www") || links.get(i).startsWith("http://"))
                external.add(links.get(i));
        return external;
    }

    private List<String> findExternalLinksWithForEachLoop() {
        List<String> external = new ArrayList<>();
        for (String link : links) {
            if (link.startsWith("www") || link.startsWith("http://"))
                external.add(link);
        }
        return external;
    }

    private List<String> findExternalLinksWithStreamLoop() {
        return links.stream().filter(l -> l.startsWith("http") || l.startsWith("www")).collect(toList());
    }
}
