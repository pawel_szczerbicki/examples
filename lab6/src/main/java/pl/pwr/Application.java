package pl.pwr;

import java.sql.DriverManager;
import java.sql.SQLException;

import static pl.pwr.ContextConfig.DRIVER;

public class Application {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
//        ApplicationContext ctx = new AnnotationConfigApplicationContext(ContextConfig.class);
//        ctx.getBean(DatabasesExample.class).show();

        Class.forName(DRIVER);
        DriverManager.getConnection("jdbc:postgresql://localhost:5432/todo", "postgres", "postgres")
                .createStatement().execute("");

    }
}
