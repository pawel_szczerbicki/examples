package com.sda.dropbox.config;

public interface Keys {

    String DROPBOX_KEY = "dropbox.key";
    String DIRECTORY = "directory";
    String EMAIL_CLIENT = "email.client";
    String EMAIL_TO = "email.to";
    String EMAIL_SUBJECT = "email.subject";
    String EMAIL_CONTENT = "email.content";
    String SENDGRID_KEY = "sendgrid.key";
}
