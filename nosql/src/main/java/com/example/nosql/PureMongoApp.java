package com.example.nosql;

import com.example.nosql.user.User;
import com.google.gson.Gson;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class PureMongoApp {
    public static void main(String[] args) {
        MongoClient mongoClient = new MongoClient();
        MongoDatabase db = mongoClient.getDatabase("test");
        User u = new User("user 2");

        Document d = new Document();
        d.append("name", "other");
        db.getCollection("user").insertOne(d);

        Gson g = new Gson();
        Document user2 = Document.parse(g.toJson(u));

    }
}
