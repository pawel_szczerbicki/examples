package pl.edu.pwr;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;

public class Example {
    public static void main(String[] args) {
         new Example().run();
    }
    public void run(){
        List<User> users = new ArrayList<>();
        users.add(new User("b", 1));
        users.add(new User("z", 3));
        users.add(new User("a", 2));
        for (int i = 0; i < users.size(); i++) {
            System.out.println(users.get(i));
        }

        for (User u :  users) {
            System.out.println(u);
        }

        Iterator<User> iterator = users.iterator();
        while (iterator.hasNext()) System.out.println(iterator.next());

        users.stream().filter(new Predicate<User>() {
            @Override
            public boolean test(User user) {
                return false;
            }
        }).collect(toList());

    }

   public class User implements Comparable<User>{
       private String surname;
       private Integer age;
       public User(String surname, int age) {
           this.surname = surname;
       }

       @Override
       public int compareTo(User o) {
           return surname.compareTo(o.surname);
       }

       public Comparator<User> AGE_COMPARATOR = new Comparator<User>() {
           @Override
           public int compare(User o1, User o2) {
               return o1.age.compareTo(o2.age) ;
           }
       };
   }
}

