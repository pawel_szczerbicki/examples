package pwr.databases.spring;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import java.net.UnknownHostException;

import static com.mongodb.MongoCredential.createCredential;
import static java.util.Collections.singletonList;

@Configuration
@ComponentScan(basePackages = "pwr.databases")
@EnableWebMvc
@EnableAsync
public class AppConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**");
    }

    @Bean
    public UrlBasedViewResolver urlBasedViewResolver(){
        UrlBasedViewResolver r = new UrlBasedViewResolver();
        r.setViewClass(JstlView.class);
        r.setPrefix("/jsp/");
        r.setSuffix(".jsp");
        return r;
    }

    @Bean
    public MongoTemplate mongoTemplate() throws UnknownHostException {
        MongoCredential credential = createCredential("admin", "dev", "admin".toCharArray());
        MongoClient c = new MongoClient(new ServerAddress("mongolab.com", 27017), singletonList(credential));
        return new MongoTemplate(new SimpleMongoDbFactory(c,"dev"));
    }

}
