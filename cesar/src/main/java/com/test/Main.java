package com.test;

import java.util.HashSet;

public class Main {

    public static void main(String[] args) {
        int a = 1;
        User u1 = new User("name", "surname", "login");
        User u2 = new User("name2", "surname2", "login2");
        System.out.println(u1.getFullName());
        System.out.println(u2.getFullName());


        FileReader reader = new FileReader();
        String digits = reader.read("/Users/pawel/Downloads/Dane_PR2/wypozyczenia.txt");

        String[] lines = digits.split("\n");

        int LP_INDEX = 0;
        int PESEL_INDEX = 1;
        int COURSE_INDEX = 2;


        HashSet<String> pesels = new HashSet<>();

        pesels.add("dw");
        pesels.add("dw");
        pesels.add("dw");
        pesels.add("dw");


        for (int i = 1; i < lines.length; i++) {

            String[] values = lines[i].split("\t");
            pesels.add(values[PESEL_INDEX]);
            Course c = new Course(Integer.parseInt(values[LP_INDEX]), values[PESEL_INDEX],values[COURSE_INDEX]);
        }
        for (String pesel : pesels) {

            System.out.println(pesel);
        }

        int b = 3;
        int c = b;


        int[] chars = {65, 79, 65};

        int offset = 3;

        cezar(chars, 100, true);
        for (int i = 0; i < chars.length; i++) {
            System.out.println(chars[i]);
        }
        cezar(chars, 100, false);
        for (int i = 0; i < chars.length; i++) {
            System.out.println(chars[i]);
        }


    }


    static int[] cezar(int[] chars, int offset, boolean encrypt) {
        for (int i = 0; i < chars.length; i++) {
            for (int k = 0; k < offset; k++) {
                if (encrypt) chars[i]++;
                else chars[i]--;
                if (chars[i] < 65) chars[i] = 90;
                if (chars[i] > 90) chars[i] = 65;
            }
        }
        return chars;

    }

}
