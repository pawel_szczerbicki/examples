package pl.pwr.chat;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChatServer {
    private Socket socket = null;
    private DataInputStream streamIn = null;
    private int port;

    public ChatServer(int port) {
        ExecutorService service = Executors.newFixedThreadPool(10);
        this.port = port;
    }

    public void start() {
        try {
            System.out.println("Binding to port " + port + ", please wait  ...");
            ServerSocket server = new ServerSocket(port);
            System.out.println("Server started: " + server);
            System.out.println("Waiting for a client ...");
            socket = server.accept();
            System.out.println("Client accepted: " + socket);
            streamIn = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
            while (true) {
                try {

                    System.out.println(streamIn.readUTF());
                } catch (IOException e) {
                    e.printStackTrace();
                    close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() throws IOException {
        if (socket != null) socket.close();
        if (streamIn != null) streamIn.close();
    }

    public static void main(String args[]) {
        new ChatServer(3333).start();
    }






}