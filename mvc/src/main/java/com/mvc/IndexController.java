package com.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

@Controller
public class IndexController {
    
    @GetMapping(value = "/new")
    public String index(Model m){
        m.addAttribute("some", Arrays.asList("aaa", "bb"));
        return "index";
    }

    @RequestMapping(value = "/example")
    @ResponseBody
    public String example(){
        return "<b> dsqdeqw</b>";
    }

    @RequestMapping("/cookie")
    @ResponseBody
    public void setCookie(HttpServletResponse response) throws IOException {

        response.addCookie(new Cookie("shark", "cookie"));

//        response.sendRedirect("/");
    }

}
