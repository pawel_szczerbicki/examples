package com.sda.interfaces;

public interface Reader {

    String read();
}
