package com.mvc.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/users")
public class UserController {


    public static void main(String[] args) {
        String a = null;
        Long b = null;
        UserController example = new UserController();
        example = null;

        example.some(a.toLowerCase(), b.intValue());

    }

    public void some(String a, int b) {

    }


    @Autowired
    private UserService userService;

    @GetMapping
    public String users(Model model, HttpServletRequest request) {
        model.addAttribute("ip", request.getRemoteAddr());
        model.addAttribute("users", userService.getAll());
        return "user";
    }

    @PostMapping
    public String saveUser(User u) {
        userService.save(u);
        return "redirect:/users";
    }


}
